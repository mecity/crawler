# MeCity Kubernetes Infrastructure
## Install tiller/helm

```
helm init
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init --service-account tiller --upgrade
```

### Docker registry
```
helm install stable/docker-registry
```

### Jenkins master

```
kubectl apply -f cicd/jenkins/jenkins.yaml
```

get password: kubectl exec POD_ID cat /var/jenkins_home/secrets/initialAdminPassword   
temp admin password: 1aee3e375fdd414c9b53195afd41eee1

Open and configure the Jenkins master as here: https://www.blazemeter.com/blog/how-to-setup-scalable-jenkins-on-top-of-a-kubernetes-cluster   

Note: You need to Restrict job run to !master for Jenkins to run jobs on slaves
