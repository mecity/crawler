module bitbucket.org/mecity/crawler

require (
	cloud.google.com/go v0.27.0 // indirect
	contrib.go.opencensus.io/exporter/stackdriver v0.6.0 // indirect
	firebase.google.com/go v3.3.0+incompatible
	github.com/PuerkitoBio/goquery v1.4.0
	github.com/Selvatico/go-mocket v1.0.4
	github.com/andybalholm/cascadia v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20181014144952-4e0d7dc8888f // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/go-sql-driver/mysql v1.3.0
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/google/martian v2.1.0+incompatible // indirect
	github.com/googleapis/gax-go v2.0.0+incompatible // indirect
	github.com/gorilla/context v0.0.0-20160226214623-1ea25387ff6f
	github.com/gorilla/mux v1.6.1
	github.com/jinzhu/gorm v1.9.1
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v0.0.0-20181116074157-8ec929ed50c3 // indirect
	github.com/joho/godotenv v1.2.0
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180511142126-bb74f1db0675
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/stretchr/testify v1.2.1
	go.opencensus.io v0.17.0 // indirect
	golang.org/x/crypto v0.0.0-20180509205747-2d027ae1dddd
	google.golang.org/api v0.0.0-20180914151801-003c13302b3e
	google.golang.org/genproto v0.0.0-20180912233945-5a2fd4cab2d6 // indirect
	google.golang.org/grpc v1.15.0 // indirect
)

go 1.13
