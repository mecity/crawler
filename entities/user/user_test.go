package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateJWT(t *testing.T) {
	user := User{
		ID:       1,
		Password: []byte("test"),
		Email:    "test@test.gr",
	}
	got, err := user.CreateJWT("test")
	assert.Nil(t, err)
	assert.NotEqual(t, "", got)
}

func TestValidateAccount(t *testing.T) {
	err := ValidateAccount("email@test.gr", "1")
	assert.Error(t, err)

	err = ValidateAccount("", "1")
	assert.Error(t, err)

	err = ValidateAccount("email_", "12345678")
	assert.Error(t, err)

	err = ValidateAccount("email@test.gr", "12345678")
	assert.Nil(t, err)
}
