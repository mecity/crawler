package mysql

import (
	"bitbucket.org/mecity/crawler/connector"
	"bitbucket.org/mecity/crawler/entities/user"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// UserStorage is the storage where we get the stored users
type UserStorage struct {
	conn connector.Connector
}

// New creates a new user storage
func New(conn connector.Connector) *UserStorage {
	return &UserStorage{
		conn: conn,
	}
}

// Create handles the creation of a user in database
func (s *UserStorage) Create(user *user.User) error {
	err := s.conn.GetDB().(*gorm.DB).Create(user).Error
	if err != nil {
		return errors.Wrap(err, "mysql/storage could not create user")
	}
	return nil
}

// Update update a specific user
func (s *UserStorage) Update(usr *user.User) error {
	s.conn.GetDB().(*gorm.DB).Model(&user.User{}).
		Where("id = ?", usr.ID).
		Updates(user.User{
			Platform:  usr.Platform,
			Version:   usr.Version,
			PushToken: usr.PushToken,
		}).First(&usr)
	if usr.ID == 0 {
		return errors.New("mysql/storage update user")
	}
	return nil
}

// FindUser searches for a user in database
func (s *UserStorage) FindUser(usr *user.User) error {
	s.conn.GetDB().(*gorm.DB).Model(&user.User{}).Where("email = ?", usr.Email).Preload("Searches", func(db *gorm.DB) *gorm.DB {
		return db.Order("searches.unread DESC").Order("searches.id DESC")
	}).First(usr)
	if usr.ID == 0 {
		return errors.New("mysql/storage could not find user: " + usr.Email)
	}
	return nil
}

// FindAllWithSearches eagerly fetches-all users from database
func (s *UserStorage) FindAllWithSearches() []user.User {
	var users []user.User
	s.conn.GetDB().(*gorm.DB).Model(&user.User{}).Preload("Searches").Preload("Searches.Classifieds").Find(&users)
	return users
}
