package mysql

import (
	"testing"

	"bitbucket.org/mecity/crawler/entities/user"
	"bitbucket.org/mecity/crawler/mocks_test"
	mocket "github.com/Selvatico/go-mocket"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	assert.NotNil(t, u)
}

func TestCreate(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	err := u.Create(&user.User{})
	assert.Nil(t, err)
}

func TestCreateError(t *testing.T) {
	connector := &mocks_test.Connector{Error: "test error"}
	u := New(connector)
	err := u.Create(&user.User{})
	assert.NotNil(t, err)
}

func TestUpdateError(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	err := u.Update(&user.User{ID: 0})
	assert.NotNil(t, err)
}

func TestUpdate(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	err := u.Update(&user.User{ID: 1})
	assert.Nil(t, err)
}

func TestFindByUser(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	err := u.FindUser(&user.User{ID: 1})
	assert.Nil(t, err)
}
func TestFindByUserError(t *testing.T) {
	connector := &mocks_test.Connector{}
	u := New(connector)
	commonReply := []map[string]interface{}{{"id": "0"}}
	mocket.Catcher.Reset().NewMock().WithReply(commonReply)
	err := u.FindUser(&user.User{ID: 1})
	assert.NotNil(t, err)
}

func TestFindAllWithSearches(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	users := s.FindAllWithSearches()
	assert.NotNil(t, users)
}
