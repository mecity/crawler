package user

import (
	"errors"
	"regexp"

	jwt "github.com/dgrijalva/jwt-go"
	"bitbucket.org/mecity/crawler/entities/search"
)

const (
	emailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
	passLength = 8

	// PlatformAndroid is the string used to flag android user
	PlatformAndroid = "android"
	// PlatformWeb is the string used to flag web user
	PlatformWeb = "web"
	// PlatformIOs is the string used to flag ios user
	PlatformIOs = "ios"
)

// User definition of a user entity
type User struct {
	ID        int             `json:"id" gorm:"primary_key"`
	Email     string          `json:"email" gorm:"unique_index:idx_email;type:varchar(100);not null"`
	Password  []byte          `json:"password" gorm:"type:binary(255);not null"`
	Platform  string          `json:"platform" gorm:"type:varchar(100);not null"`
	Version   string          `json:"version" gorm:"type:varchar(100);"`
	PushToken string          `json:"push_token" gorm:"type:varchar(256);"`
	Searches  []search.Search `json:"searches" gorm:"ForeignKey:UserID;AssociationForeignKey:ID"`
}

// CreateJWT creates the hashed JWT of a user
func (u User) CreateJWT(secret string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    u.Email,
		"password": u.Password,
		"id":       u.ID,
	})
	tokenStr, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}
	return tokenStr, nil
}

// ValidateAccount checks email and password to be according to our rules
func ValidateAccount(email, password string) error {
	re := regexp.MustCompile(emailRegex)
	validEmail := re.MatchString(email)
	if validEmail == false {
		return errors.New("email not valid")
	}

	if len(password) < passLength {
		return errors.New("password not valid")
	}

	return nil
}
