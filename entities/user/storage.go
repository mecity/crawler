package user

// Storage is the storage where the searches are stored
type Storage interface {
	Create(*User) error
	Update(*User) error
	FindUser(*User) error
	FindAllWithSearches() []User
}
