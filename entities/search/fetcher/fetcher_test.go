package fetcher

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFetch(t *testing.T) {
	f := HTMLFetcher{}
	got, error := f.Fetch("gives error")
	assert.Nil(t, got)
	assert.Error(t, error)

	f = HTMLFetcher{}
	got, error = f.Fetch("http://google.com")
	assert.Nil(t, error)
	assert.NotNil(t, got)
}
