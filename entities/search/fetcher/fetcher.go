package fetcher

import "github.com/PuerkitoBio/goquery"

// Fetcher is the interface to fetch a document to crawl
type Fetcher interface {
	Fetch(string) (*goquery.Document, error)
}

// HTMLFetcher is the struct that fetches html content from a url
type HTMLFetcher struct{}

// Fetch gets the content from a given url
func (f *HTMLFetcher) Fetch(path string) (*goquery.Document, error) {
	doc, err := goquery.NewDocument(path)
	if err != nil {
		return nil, err
	}
	return doc, nil
}
