package search

import (
	"time"

	"bitbucket.org/mecity/crawler/entities/classified"
)

// AllowedPerUser is a fixed number to limit the searches allowed for a user to have
const AllowedPerUser = 10

// Search defines the entity of a search that the service is going to crawl on
type Search struct {
	ID          int                     `json:"id" gorm:"primary_key"`
	URL         string                  `json:"url" gorm:"type:varchar(510);not null"`
	Host        string                  `json:"-" gorm:"type:varchar(100);not null"`
	Alias       string                  `json:"alias" gorm:"type:varchar(100);not null"`
	Unread      int                     `json:"unread" gorm:"type:int(11);default 0;"`
	UserID      int                     `json:"-"`
	Classifieds []classified.Classified `json:"classifieds" gorm:"ForeignKey:SearchID;AssociationForeignKey:ID"`
	DeletedAt   time.Time               `json:"-" gorm:"default:null;"`
	CreatedAt   time.Time               `json:"-" gorm:"default:null;"`
}
