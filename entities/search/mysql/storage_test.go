package mysql

import (
	"testing"

	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/mocks_test"
	mocket "github.com/Selvatico/go-mocket"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	assert.NotNil(t, s)
}

func TestCreate(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	err := s.Create(&search.Search{})
	assert.Nil(t, err)
}

func TestCreateError(t *testing.T) {
	connector := &mocks_test.Connector{Error: "test error"}
	s := New(connector)
	err := s.Create(&search.Search{})
	assert.NotNil(t, err)
}

func TestDeleteError(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	err := s.Delete(&search.Search{ID: 1, UserID: 2}, 1)
	assert.NotNil(t, err)
}

func TestDelete(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	err := s.Delete(&search.Search{ID: 1, UserID: 1}, 1)
	assert.Nil(t, err)
}

func TestUpdateError(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	search, err := s.Update(&search.Search{ID: 0})
	assert.NotNil(t, err)
	assert.Nil(t, search)
}

func TestUpdate(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	search, err := s.Update(&search.Search{ID: 1})
	assert.Nil(t, err)
	assert.NotNil(t, search)
}

func TestUpdateUnread(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	err := s.UpdateUnread(1, 1)
	assert.Nil(t, err)
}

func TestFindByIDError(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	search, err := s.FindByID(1, 1)
	assert.NotNil(t, err)
	assert.Nil(t, search)
}

func TestFindByID(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	commonReply := []map[string]interface{}{{"id": "1"}}
	mocket.Catcher.Reset().NewMock().WithReply(commonReply)
	search, err := s.FindByID(1, 1)
	assert.Nil(t, err)
	assert.NotNil(t, search)
}

func TestFindUserSearches(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	commonReply := []map[string]interface{}{{"id": "1"}}
	mocket.Catcher.Reset().NewMock().WithReply(commonReply)
	searches := s.FindUserSearches(1)
	assert.NotNil(t, searches)
}

func TestFindSearchByUserError(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	mocket.Catcher.Reset()
	search, err := s.FindSearchByUser(1, 1)
	assert.Error(t, err)
	assert.Nil(t, search)
}

func TestFindSearchByUser(t *testing.T) {
	connector := &mocks_test.Connector{}
	s := New(connector)
	commonReply := []map[string]interface{}{{"id": "1"}}
	mocket.Catcher.Reset().NewMock().WithReply(commonReply)
	search, err := s.FindSearchByUser(1, 1)
	assert.NoError(t, err)
	assert.NotNil(t, search)
}
