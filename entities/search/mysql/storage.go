package mysql

import (
	"fmt"

	"bitbucket.org/mecity/crawler/connector"
	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// SearchStorage is the storage where we get the stored searches
type SearchStorage struct {
	conn connector.Connector
}

// New creates a new search storage
func New(conn connector.Connector) *SearchStorage {
	return &SearchStorage{
		conn: conn,
	}
}

// Create handles the creation of a search in database
func (s *SearchStorage) Create(search *search.Search) error {
	err := s.conn.GetDB().(*gorm.DB).Create(search).Error
	if err != nil {
		return errors.Wrap(err, "mysql/storage create search")
	}
	return nil
}

// Delete handles the deletion of a user's search
func (s *SearchStorage) Delete(search *search.Search, userID int) error {
	s.conn.GetDB().(*gorm.DB).First(&search)
	if search.UserID == userID {

		s.conn.GetDB().(*gorm.DB).Delete(&search)
		s.conn.GetDB().(*gorm.DB).Delete(classified.Classified{}, "search_id = ?", search.ID)
		return nil
	}
	return errors.New(fmt.Sprintf("mysql/storage search %d association with user: %d", search.ID, userID))
}

// Update update a specific search
func (s *SearchStorage) Update(srch *search.Search) (*search.Search, error) {
	s.conn.GetDB().(*gorm.DB).Model(&search.Search{}).
		Where("user_id = ? AND id = ?", srch.UserID, srch.ID).
		Updates(&search.Search{
			ID:     srch.ID,
			UserID: srch.UserID,
			Alias:  srch.Alias,
			URL:    srch.URL,
			Unread: srch.Unread,
		}).Unscoped().Preload("Classifieds").First(&srch)
	if srch.ID == 0 {
		return nil, errors.New("mysql/storage update search")
	}
	return srch, nil
}

// UpdateUnread updates a specific search about the unread classifieds
func (s *SearchStorage) UpdateUnread(searchID int, count int) error {
	s.conn.GetDB().(*gorm.DB).Model(&search.Search{}).Where("id = ?", searchID).Update("unread", count)
	return s.conn.GetDB().(*gorm.DB).Error
}

// FindByID handles the query functionality for a specific search
func (s *SearchStorage) FindByID(searchID int, userID int) (*search.Search, error) {
	srch := &search.Search{Classifieds: []classified.Classified{}}
	s.conn.GetDB().(*gorm.DB).Model(&search.Search{}).Where("id = ? AND user_id = ?", searchID, userID).Preload("Classifieds").First(&srch)
	if srch.ID == 0 {
		return nil, errors.New("mysql/storage find search: " + string(searchID))
	}
	return srch, nil
}

// FindUserSearches finds all searches stored for a user
func (s *SearchStorage) FindUserSearches(userID int) []search.Search {
	var searches []search.Search
	s.conn.GetDB().(*gorm.DB).Where("user_id = ?", userID).Find(&searches)
	return searches
}

// FindSearchByUser handles the query to find a specific search of a user
func (s *SearchStorage) FindSearchByUser(searchID int, userID int) (*search.Search, error) {
	var search search.Search
	s.conn.GetDB().(*gorm.DB).Where("user_id = ? AND id = ?", userID, searchID).Preload("Classifieds").First(&search)
	if search.ID == 0 {
		return nil, errors.New(fmt.Sprintf("mysql/storage find search: %d", searchID))
	}
	return &search, nil
}
