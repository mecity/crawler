package provider

import (
	"testing"

	"bitbucket.org/mecity/crawler/entities/search/provider/spitogatos"
	"bitbucket.org/mecity/crawler/entities/search/provider/xe"
	"bitbucket.org/mecity/crawler/mocks_test"
	"github.com/stretchr/testify/assert"
)

func TestGetSearchProvider(t *testing.T) {
	assert := assert.New(t)
	cls := &mocks_test.ClassifiedStorage{}
	srs := &mocks_test.SearchStorage{}

	got, err := GetSearchProvider(cls, srs, "test")
	assert.Error(err)
	assert.Nil(got)

	got, err = GetSearchProvider(cls, srs, "https://www.xe.gr")
	assert.Nil(err)
	assert.NotNil(got)
	assert.IsType(&xe.Provider{}, got)

	got, err = GetSearchProvider(cls, srs, "https://en.spitogatos.gr")
	assert.Nil(err)
	assert.NotNil(got)
	assert.IsType(&spitogatos.Provider{}, got)
}

func TestParseUrl(t *testing.T) {
	//ParseURL(urlStr string) (host string, path string, err error)
	assert := assert.New(t)
	gotHost, gotPath, gotErr := ParseURL("testurl")
	assert.Error(gotErr)
	assert.Equal("", gotHost)
	assert.Equal("", gotPath)

	gotHost, gotPath, gotErr = ParseURL("xe.gr/testpath")
	assert.NoError(gotErr)
	assert.Equal(xe.Host, gotHost)
	assert.Equal("/testpath/?", gotPath)

	gotHost, gotPath, gotErr = ParseURL("xe.gr/testpath?page=2&otherparam=test")
	assert.NoError(gotErr)
	assert.Equal(xe.Host, gotHost)
	assert.Equal("/testpath/?&otherparam=test", gotPath)

	gotHost, gotPath, gotErr = ParseURL("spitogatos.gr/testpath")
	assert.NoError(gotErr)
	assert.Equal(spitogatos.Host, gotHost)
	assert.Equal("/testpath", gotPath)

	gotHost, gotPath, gotErr = ParseURL("spitogatos.gr/testpath/offset_30")
	assert.NoError(gotErr)
	assert.Equal(spitogatos.Host, gotHost)
	assert.Equal("/testpath/", gotPath)
}
