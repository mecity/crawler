package provider

import (
	"errors"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/search/provider/spitogatos"
	"bitbucket.org/mecity/crawler/entities/search/provider/xe"
)

// Provider is the interface for each provider we have to crawl to get the classifieds
type Provider interface {
	Crawl(*search.Search) ([]classified.Classified, []classified.Classified, error)
	Pagination(*goquery.Document) int
}

// GetSearchProvider gets the provider struct of this search
func GetSearchProvider(classifiedStorage classified.Storage, searchStorage search.Storage, host string) (Provider, error) {
	switch host {
	case xe.Host:
		return xe.New(classifiedStorage, searchStorage), nil
	case spitogatos.Host:
		return spitogatos.New(classifiedStorage, searchStorage), nil
	default:
		return nil, errors.New("couldn't find provider of search")
	}
}

// ParseURL checks if the host given is supported
// then gets the host and removes pagination from the url
func ParseURL(urlStr string) (host string, path string, err error) {
	if parts := strings.Split(urlStr, xe.Domain); len(parts) > 1 {
		u, _ := url.Parse(parts[1])
		q, _ := url.ParseQuery(u.RawQuery)
		q.Del(xe.Pagination)
		query := "?"
		for key := range q {
			query += "&" + key + "=" + q.Get(key)
		}
		return xe.Host, u.Path + "/" + query, nil
	}

	if parts := strings.Split(urlStr, spitogatos.Domain); len(parts) > 1 {
		url := strings.Split(parts[1], spitogatos.Pagination)
		return spitogatos.Host, url[0], nil
	}

	return "", "", errors.New("Host is not supported")
}
