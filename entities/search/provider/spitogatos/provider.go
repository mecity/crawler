package spitogatos

import (
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/search/fetcher"
	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
)

const (
	// Domain is Provider's domain
	Domain = "spitogatos.gr"
	// Host of spitogatos provider
	Host = "https://en.spitogatos.gr"
	// Pagination is the param of pagination in a host' url
	Pagination = "offset"
)

// Provider is the definition of a spitogatos provider
type Provider struct {
	classifiedStorage classified.Storage
	searchStorage     search.Storage
	fetcher           fetcher.Fetcher
}

// New creates a new car provider
func New(cs classified.Storage, ss search.Storage) *Provider {
	return &Provider{
		classifiedStorage: cs,
		searchStorage:     ss,
		fetcher:           &fetcher.HTMLFetcher{},
	}
}

// Pagination gets the pages number available in the provider's search
func (p *Provider) Pagination(doc *goquery.Document) int {
	num := doc.Find(".pagination").Find("li").Length()
	if num == 0 {
		return 1
	}
	if num >= 9 {
		return 50
	}
	return num
}

// Crawl does the crawling on the spitogatos pages
func (p *Provider) Crawl(search *search.Search) ([]classified.Classified, []classified.Classified, error) {
	new, all := []classified.Classified{}, []classified.Classified{}
	doc, err := p.fetcher.Fetch(search.Host + search.URL)
	if err != nil {
		return nil, nil, errors.Wrap(err, "provider/spitogatos couldn't load content")
	}

	// find the number of pages for this ad search
	pageNum := p.Pagination(doc)

	i := 1
	for i <= pageNum {

		endpoint := search.Host + search.URL + "/" + Pagination + "_" + strconv.Itoa((i-1)*10)
		if i == 1 {
			endpoint = search.Host + search.URL
		}

		doc, err := p.fetcher.Fetch(endpoint)
		if err != nil || doc.Find(".search_listing_title").Length() == 0 {
			break
		}
		i++
		// Find the review items
		doc.Find(".search_listing_title").Each(func(i int, s *goquery.Selection) {
			// For each item found get the info needed
			result := s
			parent := result.Parent().Parent()
			uri, _ := result.Find(".bd").Find("h4").Find("a").Attr("href")
			re := regexp.MustCompile("[0-9]+")
			strID := re.FindStringSubmatch(uri)
			id, _ := strconv.Atoi(strID[0])
			img, exists := parent.Find(".lazyImage").Attr("data-original")
			if exists == false {
				img, _ = parent.Find("a").Find("img").Attr("src")
			}
			title := result.Find(".bd").Find("h4").Find("a").Text()
			description := result.Find(".bd").Find("div").Last().Text()
			strPrice := re.FindStringSubmatch(result.Parent().Find(".listingPrice").Find(".xbig").Text())
			price := strPrice[0] + " €"
			uri = strings.Replace(uri, Host, "https://www.spitogatos.gr", -1)
			_, nonAgent := result.Parent().Find(".sg-icon-private").Attr("title")
			classified := classified.Classified{
				ID:          id,
				Title:       strings.TrimSpace(title),
				Provider:    search.Alias,
				Description: strings.TrimSpace(description),
				URL:         uri,
				Img:         img,
				Price:       strings.TrimSpace(price),
				SearchID:    search.ID,
				UserID:      search.UserID,
				IsNew:       true,
				Agent:       !nonAgent,
			}

			if classified.Exists(search.Classifieds) == false {
				err = p.classifiedStorage.Create(classified)
				if err != nil {
					return
				}
				new = append(new, classified)
				search.Unread++
			}

			all = append(all, classified)
		})
		p.searchStorage.UpdateUnread(search.ID, search.Unread)
	}

	return new, all, nil
}
