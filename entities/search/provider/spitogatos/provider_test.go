package spitogatos

import (
	"errors"
	"os"
	"testing"

	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/mocks_test"
	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
)

// MockFetcher mocks fetching functionality
type MockFetcher struct {
	Doc   *goquery.Document
	Error error
}

// Fetch mocks the content from a given url
func (f *MockFetcher) Fetch(path string) (*goquery.Document, error) {
	return f.Doc, f.Error
}
func TestNew(t *testing.T) {
	sp := New(&mocks_test.ClassifiedStorage{}, &mocks_test.SearchStorage{})
	assert.NotNil(t, sp)
}

func TestPagination(t *testing.T) {
	sp := New(&mocks_test.ClassifiedStorage{}, &mocks_test.SearchStorage{})

	// more than 1 page
	f, _ := os.Open("./test-data/spitogatos-pagination-less.html")
	doc, _ := goquery.NewDocumentFromReader(f)
	got := sp.Pagination(doc)
	assert.Equal(t, 1, got)

	// 2 pages
	f, _ = os.Open("./test-data/spitogatos-pagination-standard.html")
	doc, _ = goquery.NewDocumentFromReader(f)
	got = sp.Pagination(doc)
	assert.Equal(t, 6, got)

	// more than 5 pages
	f, _ = os.Open("./test-data/spitogatos-pagination-more.html")
	doc, _ = goquery.NewDocumentFromReader(f)
	got = sp.Pagination(doc)
	assert.Equal(t, 50, got)
}

func TestCrawlError(t *testing.T) {
	sp := &Provider{
		classifiedStorage: &mocks_test.ClassifiedStorage{},
		searchStorage:     &mocks_test.SearchStorage{},
		fetcher: &MockFetcher{
			Error: errors.New("test error"),
		},
	}

	s := &search.Search{}
	got, all, err := sp.Crawl(s)
	assert.Nil(t, got)
	assert.Nil(t, all)
	assert.Error(t, err)
}

func TestCrawl(t *testing.T) {
	f, _ := os.Open("./test-data/spitogatos-pagination-less.html")
	doc, _ := goquery.NewDocumentFromReader(f)

	sp := &Provider{
		classifiedStorage: &mocks_test.ClassifiedStorage{},
		searchStorage:     &mocks_test.SearchStorage{},
		fetcher: &MockFetcher{
			Doc: doc,
		},
	}

	s := &search.Search{}
	got, all, err := sp.Crawl(s)
	assert.Nil(t, err)
	assert.NotNil(t, got)
	assert.NotNil(t, all)
}
