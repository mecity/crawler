package xe

import (
	"strconv"
	"strings"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/search/fetcher"
	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
)

const (
	// Domain is Provider's domain
	Domain = "xe.gr"
	// Host of the xe provider
	Host = "https://www.xe.gr"
	// Pagination is the param of pagination in a host' url
	Pagination = "page"
)

// Provider is the definition of a xe provider (yellow pages)
type Provider struct {
	classifiedStorage classified.Storage
	searchStorage     search.Storage
	fetcher           fetcher.Fetcher
}

// New creates a new car provider
func New(cs classified.Storage, ss search.Storage) *Provider {
	return &Provider{
		classifiedStorage: cs,
		searchStorage:     ss,
		fetcher:           &fetcher.HTMLFetcher{},
	}
}

// Pagination gets the pages number available in the provider's search
func (p *Provider) Pagination(doc *goquery.Document) int {
	num := doc.Find(".page").Length()
	if num == 0 {
		return 1
	}
	if num >= 10 {
		return 50
	}
	return num
}

// Crawl does the crawling on the xe pages
func (p *Provider) Crawl(search *search.Search) ([]classified.Classified, []classified.Classified, error) {
	new, all := []classified.Classified{}, []classified.Classified{}
	doc, err := p.fetcher.Fetch(search.Host + search.URL + "&" + Pagination + "=1")

	if err != nil {
		return nil, nil, errors.Wrap(err, "provider/xe couldn't load content")
	}

	// find the number of pages for this ad search
	pageNum := p.Pagination(doc)

	i := 1
	for i <= pageNum {

		iStr := strconv.Itoa(i)
		i++
		endpoint := search.Host + search.URL + "&" + Pagination + "=" + iStr

		doc, err := p.fetcher.Fetch(endpoint)
		if err != nil || doc.Find(".r_desc").Length() == 0 {
			break
		}

		// Find the review items
		doc.Find(".r_desc").Each(func(i int, s *goquery.Selection) {
			// For each item found get the info needed
			result := s
			parent := result.Parent()
			strID, _ := parent.Attr("data-id")
			id, _ := strconv.Atoi(strID)
			title := result.Find("h2").Find("a").Text() + " " + result.Find("p").Find("a").Text()
			result.Find("p").Find("a").Remove()
			description := result.Find("p").Text()
			urlSrc, _ := result.Find("a").Attr("href")
			url := Host + urlSrc
			img, exists := parent.Find("a").Find("img").Attr("src")
			if exists == false {
				img = Host + "/property/images/thumbs/no_image_re_residence_128x96.png"
			}
			if len(strings.Split(img, Domain)) == 1 {
				img = Host + img
			}
			price := parent.Find(".r_stats").Find(".r_price").Text()
			_, agent := parent.Find(".pro_action_hotspot").Attr("href")

			// create a new structure
			classified := classified.Classified{
				ID:          id,
				Title:       strings.TrimSpace(title),
				Provider:    search.Alias,
				Description: strings.TrimSpace(description),
				URL:         url,
				Img:         img,
				Price:       price,
				SearchID:    search.ID,
				UserID:      search.UserID,
				IsNew:       true,
				Agent:       agent,
			}

			if classified.Exists(search.Classifieds) == false {
				err := p.classifiedStorage.Create(classified)
				new = append(new, classified)
				if err != nil {
					return
				}
				search.Unread++
			}
			all = append(all, classified)
		})
		p.searchStorage.UpdateUnread(search.ID, search.Unread)
	}

	return new, all, nil
}
