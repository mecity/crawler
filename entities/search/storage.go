package search

// Storage is the storage where the searches are stored
type Storage interface {
	Create(*Search) error
	Delete(*Search, int) error
	Update(*Search) (*Search, error)
	UpdateUnread(int, int) error
	FindByID(int, int) (*Search, error)
	FindUserSearches(int) []Search
	FindSearchByUser(int, int) (*Search, error)
}
