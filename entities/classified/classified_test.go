package classified

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExistsFalse(t *testing.T) {
	cl := &Classified{
		ID: 1,
	}
	cls := make([]Classified, 2)
	assert.False(t, cl.Exists(cls))
}

func TestExistsTrue(t *testing.T) {
	cl := Classified{
		ID: 1,
	}
	cls := []Classified{}
	cls = append(cls, cl)
	assert.True(t, cl.Exists(cls))
}

func TestHTML(t *testing.T) {
	cl := &Classified{
		ID:    1,
		Title: "Test Classified",
	}
	str := ""
	cl.HTML(&str)

	expected := "<div data-id='1' style='width: 100%;display: block;border-bottom: 1px solid #cccccc;padding: 7px 15px 7px 15px;position: relative;min-height: 150px;'><a href=''><img src='' style='display: inline-block;float: left;padding-right: 25px;height: 150px;position: relative;'/></a><div style='display: inline-block;padding-right: 15px;'><h2 style='margin: 10px 0;'><a style='text-decoration: none;color: #000000;font-size: 18px;' href=''>Test Classified</a></h2><span style='font-weight: bold;font-size: 18px;display: block;margin: 15px 0;'></span><p style='line-height: 1.5;margin-top: -10px;'></p></div></div>"

	assert.Equal(t, str, expected)
}
