package classified

import (
	"strconv"
	"time"
)

// Classified definition of a classified entity
type Classified struct {
	ID          int       `json:"id" gorm:"primary_key;auto_increment:false"`
	SearchID    int       `json:"-" gorm:"primary_key;auto_increment:false"`
	UserID      int       `json:"-"`
	Title       string    `json:"title"`
	Provider    string    `json:"provider"`
	Description string    `json:"description"`
	URL         string    `json:"url"`
	Img         string    `json:"img"`
	Price       string    `json:"price"`
	Agent       bool      `json:"agent"`
	IsNew       bool      `json:"is_new"`
	DeletedAt   time.Time `json:"-" gorm:"default:null;"`
	CreatedAt   time.Time `json:"-" gorm:"default:null;"`
}

// HTML produces the html body for a classified
func (c *Classified) HTML(html *string) {
	// get the classified's id in string format
	strID := strconv.Itoa(c.ID)

	// create the html for this classified to be added in the email
	*html += "<div data-id='" + strID + "' " +
		"style='width: 100%;display: block;border-bottom: 1px solid #cccccc;padding: 7px 15px 7px 15px;position: relative;min-height: 150px;'>" +
		"<a href='" + c.URL + "'><img src='" + c.Img + "' style='display: inline-block;float: left;padding-right: 25px;height: 150px;position: relative;'/></a>" +
		"<div style='display: inline-block;padding-right: 15px;'>" +
		"<h2 style='margin: 10px 0;'><a style='text-decoration: none;color: #000000;font-size: 18px;' href='" + c.URL + "'>" + c.Title + "</a></h2>" +
		"<span style='font-weight: bold;font-size: 18px;display: block;margin: 15px 0;'>" + c.Price + "</span>" +
		"<p style='line-height: 1.5;margin-top: -10px;'>" + c.Description + "</p>" +
		"</div>" +
		"</div>"
}

// Exists handles the check if the classified is existing already in a given slice
func (c *Classified) Exists(existingClassifieds []Classified) bool {
	for _, cl := range existingClassifieds {
		if cl.ID == c.ID {
			return true
		}
	}
	return false
}
