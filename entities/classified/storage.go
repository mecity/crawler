package classified

// Storage is the storage where the classifieds are stored
type Storage interface {
	Create(Classified) error
	Delete(int, int, int)
	OpenClassified(int, int, int)
	OpenSearchClassifieds(int, int)
	CountNew(int, int) int
	FindBySearch(searchID int) []Classified
}
