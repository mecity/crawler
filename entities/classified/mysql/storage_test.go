package mysql

import (
	"testing"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/mocks_test"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	assert.NotNil(t, c)
}

func TestCreate(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	err := c.Create(classified.Classified{})
	assert.Nil(t, err)
}

func TestCreateError(t *testing.T) {
	connector := &mocks_test.Connector{Error: "test error"}
	c := New(connector)
	err := c.Create(classified.Classified{})
	assert.NotNil(t, err)
}

func TestDelete(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	c.Delete(1, 1, 1)
}

func TestCountNew(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	counter := c.CountNew(1, 1)
	assert.Equal(t, 0, counter)
}

func TestOpenClassified(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	c.OpenClassified(1, 1, 1)
}

func TestFindBySearch(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	cls := c.FindBySearch(1)
	assert.NotNil(t, cls)
}

func TestOpenSearchClassifieds(t *testing.T) {
	connector := &mocks_test.Connector{}
	c := New(connector)
	c.OpenSearchClassifieds(1, 1)
}
