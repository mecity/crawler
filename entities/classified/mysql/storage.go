package mysql

import (
	"bitbucket.org/mecity/crawler/connector"
	"bitbucket.org/mecity/crawler/entities/classified"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// ClassifiedStorage is the storage where we get the stored classifieds
type ClassifiedStorage struct {
	conn connector.Connector
}

// New creates a new classified storage
func New(conn connector.Connector) *ClassifiedStorage {
	return &ClassifiedStorage{
		conn: conn,
	}
}

// Create handles the creation of a classified
func (s *ClassifiedStorage) Create(classified classified.Classified) error {
	err := s.conn.GetDB().(*gorm.DB).Create(&classified).Error
	if err != nil {
		return errors.Wrap(err, "mysql/storage could not create classified")
	}
	return nil
}

// Delete handles the deletion of a classified
func (s *ClassifiedStorage) Delete(classifiedID int, searchID int, userID int) {
	s.conn.GetDB().(*gorm.DB).Model(&classified.Classified{}).Where("id = ? AND search_id = ? AND user_id = ?", classifiedID, searchID, userID).Delete(&classified.Classified{})
}

// CountNew mocks the functionality of counting the new classifieds of a search
func (s *ClassifiedStorage) CountNew(searchID int, userID int) int {
	var count int
	s.conn.GetDB().(*gorm.DB).Model(&classified.Classified{}).Where("is_new = ? AND search_id = ? AND user_id = ?", 1, searchID, userID).Count(&count)
	return count
}

// OpenClassified handles the functionality of a classified when marked as read
func (s *ClassifiedStorage) OpenClassified(classifiedID int, searchID int, userID int) {
	s.conn.GetDB().(*gorm.DB).Model(&classified.Classified{}).Where("id = ? AND search_id = ? AND user_id = ?", classifiedID, searchID, userID).Update("is_new", false)
}

// OpenSearchClassifieds handles the functionality to mark as read all the classifieds of a specific search
func (s *ClassifiedStorage) OpenSearchClassifieds(userID int, searchID int) {
	s.conn.GetDB().(*gorm.DB).Model(&classified.Classified{}).Where("search_id = ? AND user_id = ?", searchID, userID).Update("is_new", false)
}

// FindBySearch handles the select query on classifieds by search ID
func (s *ClassifiedStorage) FindBySearch(searchID int) []classified.Classified {
	var classifieds []classified.Classified
	s.conn.GetDB().(*gorm.DB).Where("search_id = ?", searchID).Order("id desc").Find(&classifieds)
	return classifieds
}
