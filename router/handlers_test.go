package router

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/search/provider/xe"
	"bitbucket.org/mecity/crawler/entities/user"
	"bitbucket.org/mecity/crawler/mocks_test"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
)

func TestCreateAccount(t *testing.T) {
	tests := []struct {
		storage    user.Storage
		params     map[string]string
		headers    map[string]string
		returnCode int
	}{
		{
			storage:    &mocks_test.UserStorage{},
			params:     map[string]string{},
			headers:    map[string]string{},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params:  map[string]string{},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"email": "sotiris@test.gr",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"password": "test_password",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"email":    "sotiris@test_test.gr",
				"password": "test_password",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"email":    "sotiris@test.gr",
				"password": "1",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{Error: "test error"},
			params: map[string]string{
				"email":    "sotiris@test.gr",
				"password": "test_password",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusNotAcceptable,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"email":    "sotiris@test.gr",
				"password": "test_password",
			},
			headers: map[string]string{
				"Platform": user.PlatformAndroid,
			},
			returnCode: http.StatusCreated,
		},
	}

	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			UserStorage: test.storage,
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		form := url.Values{}
		for key, value := range test.params {
			form.Add(key, value)
		}
		req, _ := http.NewRequest("POST", "/signup", strings.NewReader(form.Encode()))
		req.PostForm = form
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		for key, value := range test.headers {
			req.Header.Add(key, value)
		}

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestLogin(t *testing.T) {
	dummyPass, _ := bcrypt.GenerateFromPassword([]byte("test_password"), bcrypt.DefaultCost)
	tests := []struct {
		storage    user.Storage
		params     map[string]string
		headers    map[string]string
		returnCode int
	}{
		{
			storage:    &mocks_test.UserStorage{},
			params:     map[string]string{},
			headers:    map[string]string{},
			returnCode: http.StatusBadRequest,
		},
		{
			storage:    &mocks_test.UserStorage{},
			params:     map[string]string{},
			headers:    map[string]string{"Platform": user.PlatformAndroid},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"email": "sotiris@test.test.com",
			},
			headers:    map[string]string{"Platform": user.PlatformAndroid},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{},
			params: map[string]string{
				"password": "test_password",
			},
			headers:    map[string]string{"Platform": user.PlatformAndroid},
			returnCode: http.StatusBadRequest,
		},
		{
			storage: &mocks_test.UserStorage{Error: "test error"},
			params: map[string]string{
				"email":    "sotiris@test.test.com",
				"password": "test_password",
			},
			headers:    map[string]string{"Platform": user.PlatformAndroid},
			returnCode: http.StatusForbidden,
		},
		{
			storage: &mocks_test.UserStorage{
				Response: user.User{
					Email:    "sotiris@test.test.com",
					Password: dummyPass,
					Searches: []search.Search{{ID: 1}, {ID: 2}},
				},
			},
			params: map[string]string{
				"email":    "sotiris@test.test.com",
				"password": "test_password",
			},
			headers:    map[string]string{"Platform": user.PlatformAndroid},
			returnCode: http.StatusAccepted,
		},
	}

	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			UserStorage: test.storage,
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		form := url.Values{}
		for key, value := range test.params {
			form.Add(key, value)
		}
		req, _ := http.NewRequest("POST", "/login", strings.NewReader(form.Encode()))
		req.PostForm = form
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		for key, value := range test.headers {
			req.Header.Add(key, value)
		}

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestValidateTokens(t *testing.T) {
	// token :=
	token, _ := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	}).SignedString([]byte("secret"))
	validToken := "Bearer " + token
	nonValidToken := "Bearer not-valid"
	emptyToken := ""

	tests := []struct {
		searchStorage search.Storage
		userStorage   user.Storage
		token         string
		returnCode    int
	}{
		{
			searchStorage: &mocks_test.SearchStorage{},
			userStorage:   &mocks_test.UserStorage{Error: "test error"},
			token:         validToken,
			returnCode:    http.StatusForbidden,
		},
		{
			searchStorage: &mocks_test.SearchStorage{},
			userStorage:   &mocks_test.UserStorage{},
			token:         nonValidToken,
			returnCode:    http.StatusForbidden,
		},
		{
			searchStorage: &mocks_test.SearchStorage{},
			userStorage:   &mocks_test.UserStorage{},
			token:         emptyToken,
			returnCode:    http.StatusForbidden,
		},
		{
			searchStorage: &mocks_test.SearchStorage{},
			userStorage:   &mocks_test.UserStorage{},
			token:         validToken,
			returnCode:    http.StatusOK,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage: test.searchStorage,
			UserStorage:   test.userStorage,
			Secret:        "secret",
		}

		m := &MiddleWare{UserStorage: test.userStorage}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("GET", "/searches", nil)
		req.Header.Add("Authorization", test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestGetSearches(t *testing.T) {
	token, _ := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	}).SignedString([]byte("secret"))

	tests := []struct {
		searchStorage search.Storage
		userStorage   user.Storage
		token         string
		returnCode    int
	}{
		{
			searchStorage: &mocks_test.SearchStorage{},
			userStorage:   &mocks_test.UserStorage{Error: "test error"},
			token:         token,
			returnCode:    http.StatusForbidden,
		},
		{
			searchStorage: &mocks_test.SearchStorage{Responses: []search.Search{{ID: 1}, {ID: 2}}},
			userStorage:   &mocks_test.UserStorage{},
			token:         token,
			returnCode:    http.StatusOK,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage: test.searchStorage,
			UserStorage:   test.userStorage,
			Secret:        "secret",
		}

		m := &MiddleWare{UserStorage: test.userStorage}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("GET", "/searches", nil)
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestCreateSearch(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "1",
		"password": "",
		"id":       "",
		"searches": "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))
	searches := make([]search.Search, 10, 10)
	token = jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "1",
		"password": "",
		"id":       "",
		"searches": searches,
	})
	tokenStrWithSearches, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		userStorage   user.Storage
		searchStorage search.Storage
		token         string
		params        map[string]string
		returnCode    int
	}{
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			params:        map[string]string{},
			returnCode:    http.StatusBadRequest,
		},
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			params: map[string]string{
				"url": "test_url",
			},
			returnCode: http.StatusBadRequest,
		},
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			params: map[string]string{
				"alias": "test alias",
			},
			returnCode: http.StatusBadRequest,
		},
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			params: map[string]string{
				"url": xe.Host + "/test url",
			},
			returnCode: http.StatusBadRequest,
		},
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{WriteError: "error message"},
			token:         tokenStr,
			params: map[string]string{
				"url":   xe.Host + "/test url",
				"alias": "test alias",
			},
			returnCode: http.StatusInternalServerError,
		},
		{
			userStorage:   &mocks_test.UserStorage{},
			searchStorage: &mocks_test.SearchStorage{Responses: []search.Search{{ID: 1}, {ID: 2}}},
			token:         tokenStr,
			params: map[string]string{
				"url":   xe.Host + "/test url",
				"alias": "test alias",
			},
			returnCode: http.StatusCreated,
		},
		{
			userStorage: &mocks_test.UserStorage{
				Response: user.User{
					Searches: make([]search.Search, 10, 10),
				},
			},
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStrWithSearches,
			params: map[string]string{
				"url":   xe.Host + "/test url",
				"alias": "test alias",
			},
			returnCode: http.StatusNotAcceptable,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			UserStorage:   test.userStorage,
			SearchStorage: test.searchStorage,
			Secret:        "secret",
		}

		form := url.Values{}
		for key, value := range test.params {
			form.Add(key, value)
		}

		m := &MiddleWare{UserStorage: test.userStorage}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("POST", "/search", strings.NewReader(form.Encode()))
		req.PostForm = form
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestDeleteSearch(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		id            string
		searchStorage search.Storage
		token         string
		returnCode    int
	}{
		{
			id:            "testerror",
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			returnCode:    http.StatusBadRequest,
		},
		{
			id:            "1",
			searchStorage: &mocks_test.SearchStorage{WriteError: "error message"},
			token:         tokenStr,
			returnCode:    http.StatusBadRequest,
		},
		{
			id:            "1",
			searchStorage: &mocks_test.SearchStorage{},
			token:         tokenStr,
			returnCode:    http.StatusNoContent,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage: test.searchStorage,
			Secret:        "secret",
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("DELETE", "/search/"+test.id, nil)
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestGetSearch(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		id                string
		searchStorage     search.Storage
		classifiedStorage classified.Storage
		token             string
		returnCode        int
	}{
		{
			id:                "testerror",
			searchStorage:     &mocks_test.SearchStorage{Response: &search.Search{}},
			classifiedStorage: &mocks_test.ClassifiedStorage{Response: []classified.Classified{}},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchStorage:     &mocks_test.SearchStorage{ReadError: "error message"},
			classifiedStorage: &mocks_test.ClassifiedStorage{Response: []classified.Classified{}},
			token:             tokenStr,
			returnCode:        http.StatusInternalServerError,
		},
		{
			id:                "1",
			searchStorage:     &mocks_test.SearchStorage{Response: &search.Search{}},
			classifiedStorage: &mocks_test.ClassifiedStorage{Response: []classified.Classified{}},
			token:             tokenStr,
			returnCode:        http.StatusOK,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage:     test.searchStorage,
			ClassifiedStorage: test.classifiedStorage,
			Secret:            "secret",
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("GET", "/search/"+test.id, nil)
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestReadSearch(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		id                string
		searchStorage     search.Storage
		classifiedStorage classified.Storage
		token             string
		returnCode        int
	}{
		{
			id:                "testerror",
			searchStorage:     &mocks_test.SearchStorage{Response: &search.Search{}},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchStorage:     &mocks_test.SearchStorage{ReadError: "error message"},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchStorage:     &mocks_test.SearchStorage{Response: &search.Search{}, WriteError: "error message"},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchStorage:     &mocks_test.SearchStorage{Response: &search.Search{Classifieds: []classified.Classified{{ID: 1, IsNew: true}, {ID: 2, IsNew: false}}}},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusAccepted,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage:     test.searchStorage,
			ClassifiedStorage: test.classifiedStorage,
			Secret:            "secret",
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("PUT", "/search/"+test.id, strings.NewReader(""))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestDeleteClassified(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		id                string
		searchID          string
		searchStorage     search.Storage
		classifiedStorage classified.Storage
		token             string
		returnCode        int
	}{
		{
			id:                "testerror",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "testerror",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{ReadError: "error message"},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusOK,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage:     test.searchStorage,
			ClassifiedStorage: test.classifiedStorage,
			Secret:            "secret",
		}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("DELETE", "/classified/"+test.searchID+"/"+test.id, nil)
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}

func TestOpenClassified(t *testing.T) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":    "",
		"password": "",
		"id":       "",
	})
	tokenStr, _ := token.SignedString([]byte("secret"))

	tests := []struct {
		id                string
		searchID          string
		searchStorage     search.Storage
		classifiedStorage classified.Storage
		token             string
		returnCode        int
	}{
		{
			id:                "testerror",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "testerror",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{WriteError: "error message"},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusBadRequest,
		},
		{
			id:                "1",
			searchID:          "1",
			searchStorage:     &mocks_test.SearchStorage{},
			classifiedStorage: &mocks_test.ClassifiedStorage{},
			token:             tokenStr,
			returnCode:        http.StatusAccepted,
		},
	}
	for _, test := range tests {
		assert := assert.New(t)
		httpHandler := &HTTPHandler{
			SearchStorage:     test.searchStorage,
			ClassifiedStorage: test.classifiedStorage,
			Secret:            "secret",
		}

		form := url.Values{}

		m := &MiddleWare{UserStorage: &mocks_test.UserStorage{}}
		r := New(m, httpHandler)
		req, _ := http.NewRequest("PUT", "/classified/"+test.searchID+"/"+test.id, strings.NewReader(form.Encode()))
		req.PostForm = form
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Add("Authorization", "Bearer "+test.token)

		res := httptest.NewRecorder()
		r.ServeHTTP(res, req)
		assert.Equal(test.returnCode, res.Code)
	}
}
