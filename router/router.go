package router

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
)

// New creates a router
func New(m *MiddleWare, h *HTTPHandler) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range Routes(m, h) {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}
	return router
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithStatus(w http.ResponseWriter, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// getParameter gets the parameter of the request
func getParameter(w http.ResponseWriter, r *http.Request, key string, required bool) (string, error) {
	param := r.FormValue(key)

	// validate that email parameter was given
	if param == "" && required == true {
		err := "Parameter " + key + " is missing"
		return "", errors.New(err)
	}

	return param, nil
}
