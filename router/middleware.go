package router

import (
	"errors"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"bitbucket.org/mecity/crawler/entities/user"
	"github.com/mitchellh/mapstructure"
)

// MiddleWare defines the struct that validates every request when necessary
type MiddleWare struct {
	UserStorage user.Storage
}

// Validate identifies if the client making a request is a valid one
func (m *MiddleWare) Validate(next http.HandlerFunc, secret string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, errors.New("There was an error when validating request")
					}
					return []byte(secret), nil
				})
				if err != nil {
					respondWithError(w, http.StatusForbidden, err.Error())
					return
				}
				if token.Valid {
					var user user.User
					context.Set(req, "decoded", token.Claims)
					mapstructure.Decode(token.Claims, &user)
					user.ID = 0
					err := m.UserStorage.FindUser(&user)
					if err != nil {
						respondWithError(w, http.StatusForbidden, "Invalid authorization token")
						return
					}
					next(w, req)
				} else {
					respondWithError(w, http.StatusForbidden, "Invalid authorization token")
				}
			}
		} else {
			respondWithError(w, http.StatusForbidden, "An authorization header is required")
		}
	})
}
