package router

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/search/provider"
	"bitbucket.org/mecity/crawler/entities/user"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	"golang.org/x/crypto/bcrypt"
)

// HTTPHandler definition of the http endpoint handler
type HTTPHandler struct {
	UserStorage       user.Storage
	SearchStorage     search.Storage
	ClassifiedStorage classified.Storage
	Secret            string
}

// CreateAccount for signing up new users
func (h HTTPHandler) CreateAccount(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	platform := strings.ToLower(r.Header.Get("Platform"))
	switch platform {
	case user.PlatformAndroid:
	case user.PlatformIOs:
	case user.PlatformWeb:
		break
	default:
		platform = user.PlatformWeb
	}

	email, err := getParameter(w, r, "email", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create account. Missing email parameter...")
		return
	}
	password, err := getParameter(w, r, "password", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create account. Missing password parameter...")
		return
	}

	err = user.ValidateAccount(email, password)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create account because "+err.Error())
		return
	}

	push, _ := getParameter(w, r, "push_token", false)
	version, _ := getParameter(w, r, "version", false)

	// Generate "hash" to store from user password
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create account. Try again later...")
		return
	}

	u := user.User{
		Email:     email,
		Password:  hashedPass,
		Platform:  platform,
		PushToken: push,
		Version:   version,
	}

	err = h.UserStorage.Create(&u)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusNotAcceptable, "Couldn't create account. Email already in use?")
		return
	}

	token, err := u.CreateJWT(h.Secret)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, "Couldn't create account. Try again later...")
		return
	}

	response := struct {
		Token    string   `json:"token"`
		Searches []string `json:"searches"`
	}{Token: token, Searches: []string{}}

	respondWithJSON(w, http.StatusCreated, response)
}

// Login handles the login functionality of a user
func (h HTTPHandler) Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	platform := strings.ToLower(r.Header.Get("Platform"))
	switch platform {
	case user.PlatformAndroid:
	case user.PlatformIOs:
	case user.PlatformWeb:
		break
	default:
		platform = user.PlatformWeb
	}

	email, err := getParameter(w, r, "email", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't login to account. Missing email parameter...")
		return
	}
	password, err := getParameter(w, r, "password", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't login to account. Missing password parameter...")
		return
	}

	push, _ := getParameter(w, r, "push_token", false)
	version, _ := getParameter(w, r, "version", false)

	// Generate "hash" to store from user password
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't login. Try again later...")
		return
	}

	u := &user.User{
		Email:    email,
		Password: hashedPass,
		Searches: []search.Search{},
	}
	err = h.UserStorage.FindUser(u)
	if err != nil {
		respondWithError(w, http.StatusForbidden, err.Error())
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if err != nil {
		respondWithError(w, http.StatusForbidden, "Couldn't login")
		return
	}

	token, err := u.CreateJWT(h.Secret)
	if err != nil {
		respondWithError(w, http.StatusForbidden, "Couldn't login")
		return
	}

	u.Version = version
	u.PushToken = push
	u.Platform = platform
	h.UserStorage.Update(u)

	type Search struct {
		ID     int    `json:"id"`
		Alias  string `json:"alias"`
		Host   string `json:"host"`
		Unread int    `json:"unread"`
	}
	searches := make([]Search, len(u.Searches))
	for i, srch := range u.Searches {
		searches[i] = Search{
			ID:     srch.ID,
			Alias:  srch.Alias,
			Host:   srch.Host,
			Unread: srch.Unread,
		}
	}

	response := struct {
		Token    string   `json:"token"`
		Searches []Search `json:"searches"`
	}{Token: token, Searches: searches}

	respondWithJSON(w, http.StatusAccepted, response)
}

// GetSearches retrieves all the user's searches
func (h HTTPHandler) GetSearches(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)

	srchs := h.SearchStorage.FindUserSearches(user.ID)
	type Search struct {
		ID     int    `json:"id"`
		Alias  string `json:"alias"`
		Host   string `json:"host"`
		Unread int    `json:"unread"`
	}
	searches := make([]Search, len(srchs))
	for i, srch := range srchs {
		searches[i] = Search{
			ID:     srch.ID,
			Alias:  srch.Alias,
			Host:   srch.Host,
			Unread: srch.Unread,
		}
	}
	response := struct {
		Searches []Search `json:"searches"`
	}{Searches: searches}

	respondWithJSON(w, http.StatusOK, response)
}

// CreateSearch creates a new search with data given by user
func (h HTTPHandler) CreateSearch(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)

	url, err := getParameter(w, r, "url", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create search. Missing url parameter...")
		return
	}

	host, url, err := provider.ParseURL(url)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	alias, err := getParameter(w, r, "alias", true)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't create search. Missing alias parameter...")
		return
	}

	r.ParseForm()

	if len(user.Searches) == 10 {
		respondWithError(w, http.StatusNotAcceptable, "You are not allowed to add a new search...")
		return
	}

	search := &search.Search{
		URL:    url,
		Alias:  alias,
		Host:   host,
		UserID: user.ID,
	}

	err = h.SearchStorage.Create(search)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	srchs := h.SearchStorage.FindUserSearches(user.ID)
	type Search struct {
		ID     int    `json:"id"`
		Alias  string `json:"alias"`
		Host   string `json:"host"`
		Unread int    `json:"unread"`
	}
	searches := make([]Search, len(srchs))
	for i, srch := range srchs {
		searches[i] = Search{
			ID:     srch.ID,
			Alias:  srch.Alias,
			Host:   srch.Host,
			Unread: srch.Unread,
		}
	}
	response := struct {
		Searches []Search `json:"searches"`
	}{Searches: searches}

	respondWithJSON(w, http.StatusCreated, response)
}

// DeleteSearch deletes a user's search
func (h HTTPHandler) DeleteSearch(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}

	err = h.SearchStorage.Delete(&search.Search{ID: id}, user.ID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}
	respondWithStatus(w, http.StatusNoContent)
}

// GetSearch retrieves the search requested from the user
func (h HTTPHandler) GetSearch(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	vars := mux.Vars(r)
	searchID, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}

	search, err := h.SearchStorage.FindSearchByUser(searchID, user.ID)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "Couldn't find search. Try again later...")
		return
	}
	respondWithJSON(w, http.StatusOK, search)
}

// ReadSearch marks as read search and its classifieds
func (h HTTPHandler) ReadSearch(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	vars := mux.Vars(r)
	searchID, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}

	search, err := h.SearchStorage.FindSearchByUser(searchID, user.ID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't find search. Try again later...")
		return
	}
	err = h.SearchStorage.UpdateUnread(searchID, 0)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't update search. Try again later...")
		return
	}
	h.ClassifiedStorage.OpenSearchClassifieds(user.ID, searchID)
	search.Unread = 0
	for i := 0; i < len(search.Classifieds); i++ {
		cl := &search.Classifieds[i]
		cl.IsNew = false
	}

	respondWithJSON(w, http.StatusAccepted, search)
}

// DeleteClassified deletes a specific classified of a user
func (h HTTPHandler) DeleteClassified(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)

	vars := mux.Vars(r)
	classifiedID, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid classified ID")
		return
	}
	searchID, err := strconv.Atoi(vars["search_id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}

	h.ClassifiedStorage.Delete(classifiedID, searchID, user.ID)

	count := h.ClassifiedStorage.CountNew(searchID, user.ID)
	h.SearchStorage.UpdateUnread(searchID, count)

	search, err := h.SearchStorage.FindByID(searchID, user.ID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Couldn't find search. Try again later...")
		return
	}
	respondWithJSON(w, http.StatusOK, search)
}

// OpenClassified marks a classified as viewed
func (h HTTPHandler) OpenClassified(w http.ResponseWriter, r *http.Request) {
	decoded := context.Get(r, "decoded")
	var user user.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)

	vars := mux.Vars(r)
	classifiedID, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid classified ID")
		return
	}
	searchID, err := strconv.Atoi(vars["search_id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}

	h.ClassifiedStorage.OpenClassified(classifiedID, searchID, user.ID)
	count := h.ClassifiedStorage.CountNew(searchID, user.ID)
	err = h.SearchStorage.UpdateUnread(searchID, count)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid search ID")
		return
	}
	respondWithStatus(w, http.StatusAccepted)
}
