package router

import (
	"net/http"
)

// Route definition of an endpoint
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes retrieves all the endpoints allowed
func Routes(m *MiddleWare, h *HTTPHandler) []Route {
	return []Route{
		// All User Routes
		{"CreateAccount", "POST", "/signup", h.CreateAccount},
		{"Login", "POST", "/login", h.Login},

		// All Search Routes
		{"GetSearches", "GET", "/searches", m.Validate(h.GetSearches, h.Secret)},
		{"CreateSearch", "POST", "/search", m.Validate(h.CreateSearch, h.Secret)},
		{"ReadSearch", "PUT", "/search/{id}", m.Validate(h.ReadSearch, h.Secret)},
		{"DeleteSearch", "DELETE", "/search/{id}", m.Validate(h.DeleteSearch, h.Secret)},
		{"GetSearch", "GET", "/search/{id}", m.Validate(h.GetSearch, h.Secret)},

		// All Classified Routes
		{"OpenClassified", "PUT", "/classified/{search_id}/{id}", m.Validate(h.OpenClassified, h.Secret)},
		{"DeleteClassified", "DELETE", "/classified/{search_id}/{id}", m.Validate(h.DeleteClassified, h.Secret)},
	}
}
