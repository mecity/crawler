package mysql

import (
	"fmt"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/search"
	"bitbucket.org/mecity/crawler/entities/user"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// Connector contains a gorm.DB connection to mysql
type Connector struct {
	connStr string
	dbType  string
	db      *gorm.DB
}

// New initiases a connection to mysql and assigns it to a new Storage struct
func New(username string, password string, host string, port string, db string) *Connector {

	var cred string
	// [username[:password]@]
	if username != "" {
		cred = username
		if password != "" {
			cred = cred + ":" + password
		}
		cred = cred + "@"
	}

	connectStr := fmt.Sprintf("%stcp([%s]:%s)/%s", cred, host, port, db)

	connector := &Connector{
		connStr: connectStr,
		dbType:  "mysql",
	}
	return connector

}

// Connect handles a new connection with mysql and assigns it to Connector struct
func (c *Connector) Connect() error {
	conn, err := gorm.Open(c.dbType, c.connStr+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return errors.Wrap(err, "mysql/storage connection")
	}
	c.db = conn
	c.db.DB().SetMaxIdleConns(10)
	//db.LogMode(true)
	c.db.AutoMigrate(&user.User{}, &search.Search{}, &classified.Classified{})

	return nil
}

// Close handles a close connection with mysql and assigns it to Connector struct
func (c *Connector) Close() {
	c.db.Close()
}

// GetDB returns the connection made with the appropriate database
func (c *Connector) GetDB() interface{} {
	return c.db
}
