package mysql

import (
	"testing"

	mocket "github.com/Selvatico/go-mocket"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	c := New("test", "test", "test", "test", "test")
	assert.NotNil(t, c)
	assert.Equal(t, "test:test@tcp([test]:test)/test", c.connStr)
}

func TestGetDB(t *testing.T) {
	expected := &gorm.DB{}
	c := &Connector{
		db: expected,
	}
	assert.Equal(t, expected, c.GetDB())
}

func TestClose(t *testing.T) {
	mocket.Catcher.Register()
	// GORM
	db, _ := gorm.Open(mocket.DRIVER_NAME, "any_string")
	c := &Connector{
		db: db,
	}
	c.Close()
}

func TestConnectError(t *testing.T) {
	c := New("test", "test", "test", "test", "test")
	err := c.Connect()
	assert.NotNil(t, err)
}

func TestConnect(t *testing.T) {
	mocket.Catcher.Register()
	c := &Connector{
		connStr: "any_string",
		dbType:  mocket.DRIVER_NAME,
	}
	err := c.Connect()
	assert.Nil(t, err)
}

func getMockConnection() *gorm.DB {
	mocket.Catcher.Register()
	// GORM
	db, _ := gorm.Open(mocket.DRIVER_NAME, "any_string")
	return db
}
