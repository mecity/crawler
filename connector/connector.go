package connector

// Connector is an interface for the database connection
type Connector interface {
	Connect() error
	Close()
	GetDB() interface{}
}
