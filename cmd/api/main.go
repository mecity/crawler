package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"bitbucket.org/mecity/crawler/connector/mysql"
	classifiedStore "bitbucket.org/mecity/crawler/entities/classified/mysql"
	searchStore "bitbucket.org/mecity/crawler/entities/search/mysql"
	userStore "bitbucket.org/mecity/crawler/entities/user/mysql"
	"bitbucket.org/mecity/crawler/router"
	"github.com/pkg/errors"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		// Info that no .env file was provided for overload
		fmt.Println("Did't load any .env configuration file")
	}
}

func main() {
	token := os.Getenv("SEARCH_TOKEN_SECRET")
	if token == "" {
		fmt.Println("Missing configuration SEARCH_TOKEN_SECRET")
		os.Exit(1)
	}

	dbUser := os.Getenv("SEARCH_DB_USER")
	if dbUser == "" {
		fmt.Println("Missing configuration SEARCH_DB_USER")
		os.Exit(1)
	}

	dbPass := os.Getenv("SEARCH_DB_PASS")
	if dbPass == "" {
		fmt.Println("Missing configuration SEARCH_DB_PASS")
		os.Exit(1)
	}

	dbHost := os.Getenv("SEARCH_DB_HOST")
	if dbHost == "" {
		fmt.Println("Missing configuration SEARCH_DB_HOST")
		os.Exit(1)
	}

	dbPort := os.Getenv("SEARCH_DB_PORT")
	if dbPort == "" {
		fmt.Println("Missing configuration SEARCH_DB_PORT")
		os.Exit(1)
	}

	dbName := os.Getenv("SEARCH_DB_DB")
	if dbName == "" {
		fmt.Println("Missing configuration SEARCH_DB_DB")
		os.Exit(1)
	}

	connector := mysql.New(dbUser, dbPass, dbHost, dbPort, dbName)
	err := connector.Connect()
	if err != nil {
		errors.Wrap(err, "Couldn't init connection")
		fmt.Println(err)
		os.Exit(1)
	}
	defer connector.Close()

	userStorage := userStore.New(connector)
	searchStorage := searchStore.New(connector)
	classifiedStorage := classifiedStore.New(connector)

	m := &router.MiddleWare{
		UserStorage: userStorage,
	}

	h := &router.HTTPHandler{
		UserStorage:       userStorage,
		SearchStorage:     searchStorage,
		ClassifiedStorage: classifiedStorage,
		Secret:            token,
	}

	router := router.New(m, h)
	log.Fatal(http.ListenAndServe(":8089", router))
}
