package main

import (
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/mecity/crawler/connector"
	"bitbucket.org/mecity/crawler/connector/mysql"
	"bitbucket.org/mecity/crawler/entities/classified"
	classifiedStore "bitbucket.org/mecity/crawler/entities/classified/mysql"
	"bitbucket.org/mecity/crawler/entities/search"
	searchStore "bitbucket.org/mecity/crawler/entities/search/mysql"
	"bitbucket.org/mecity/crawler/entities/search/provider"
	"bitbucket.org/mecity/crawler/entities/user"
	userStore "bitbucket.org/mecity/crawler/entities/user/mysql"
	"bitbucket.org/mecity/crawler/notification"
	"bitbucket.org/mecity/crawler/notification/mail"
	"bitbucket.org/mecity/crawler/notification/push"
	"github.com/joho/godotenv"
)

var con connector.Connector
var ss search.Storage
var cs classified.Storage
var ps push.Sender
var ms mail.Sender

const executionInterval int = 5

func init() {
	godotenv.Load()

	dbUser := os.Getenv("SEARCH_DB_USER")
	if dbUser == "" {
		log.Println("Logger: Missing configuration SEARCH_DB_USER")
		os.Exit(1)
	}

	dbPass := os.Getenv("SEARCH_DB_PASS")
	if dbPass == "" {
		log.Println("Logger: Missing configuration SEARCH_DB_PASS")
		os.Exit(1)
	}

	dbHost := os.Getenv("SEARCH_DB_HOST")
	if dbHost == "" {
		log.Println("Logger: Missing configuration SEARCH_DB_HOST")
		os.Exit(1)
	}

	dbPort := os.Getenv("SEARCH_DB_PORT")
	if dbPort == "" {
		log.Println("Logger: Missing configuration SEARCH_DB_PORT")
		os.Exit(1)
	}

	dbName := os.Getenv("SEARCH_DB_DB")
	if dbName == "" {
		log.Println("Logger: Missing configuration SEARCH_DB_DB")
		os.Exit(1)
	}

	con = mysql.New(dbUser, dbPass, dbHost, dbPort, dbName)
	err := con.Connect()
	if err != nil {
		log.Printf("Logger: %v\n", err)
		os.Exit(1)
	}

	ms = mail.NewSender()
	ps = push.NewSender()

	ss = searchStore.New(con)
	cs = classifiedStore.New(con)
}

func main() {

	routine := func() {
		if (time.Now().Minute() % executionInterval) != 0 {
			return
		}
		users := userStore.New(con).FindAllWithSearches()

		var wg sync.WaitGroup
		wg.Add(len(users))
		for _, user := range users {
			results := make(chan classified.Classified)
			done := make(chan struct{})

			go handleUser(user, results, done, &wg)
			go crawl(user.Searches, results, done)
		}

		wg.Wait()
		log.Println("Logger: Finished...")
	}

	// run the main functionality every minute
	for {
		time.Sleep(time.Minute)
		routine()
	}
}

func handleUser(user user.User, results chan classified.Classified, done chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	log.Println("Logger: Running for user " + user.Email)
	classifieds := []classified.Classified{}

	for {
		select {
		case item := <-results:
			classifieds = append(classifieds, item)
		case <-done:
			log.Println("Logger: Job ran for " + user.Email + " with " + strconv.Itoa(len(classifieds)) + " classifieds")
			notification.Send(&user, classifieds, ms, ps)
			return
		default:
			time.Sleep(time.Second)
		}
	}
}

func crawl(searches []search.Search, classified chan classified.Classified, done chan struct{}) {
	var wg sync.WaitGroup
	wg.Add(len(searches))

	for _, srch := range searches {
		go func(srch search.Search) {
			defer wg.Done()
			provider, err := provider.GetSearchProvider(cs, ss, srch.Host)
			if err != nil {
				log.Println("Error: " + err.Error())
				return
			}
			results, all, err := provider.Crawl(&srch)
			if err != nil {
				log.Println("Error: " + err.Error())
				return
			}
			if results != nil && len(results) > 0 {
				for _, item := range results {
					classified <- item
				}
			}

			// delete inactive/old/removed classifieds of the search
			for _, oldCl := range srch.Classifieds {
				found := false
				for _, cl := range all {
					if oldCl.ID == cl.ID {
						found = true
						break
					}
				}
				if found == false {
					if oldCl.IsNew == true {
						srch.Unread--
					}
					cs.Delete(oldCl.ID, srch.ID, oldCl.UserID)
				}
			}
			ss.UpdateUnread(srch.ID, srch.Unread)
		}(srch)
	}

	wg.Wait()
	close(done)
}
