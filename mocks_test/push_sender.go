package mocks_test

import "bitbucket.org/mecity/crawler/entities/user"

// PushSender is the struct definition of a mock push notification service
type PushSender struct {
	Error error
}

// Send handles the functionality when sending a mock push notification
func (s *PushSender) Send(recipient *user.User, title, body string) error {
	if s.Error != nil {
		return s.Error
	}
	return nil
}
