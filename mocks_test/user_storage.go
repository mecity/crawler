package mocks_test

import (
	"errors"

	"bitbucket.org/mecity/crawler/entities/user"
)

// UserStorage mocks the storage where we get the stored users
type UserStorage struct {
	Error     string
	Response  user.User
	Responses []user.User
}

// Create mocks the creation of a user in database
func (s *UserStorage) Create(user *user.User) error {
	if s.Error != "" {
		return errors.New(s.Error)
	}
	return nil
}

// Update mocks the update of a user in database
func (s *UserStorage) Update(user *user.User) error {
	if s.Error != "" {
		return errors.New(s.Error)
	}
	return nil
}

// FindUser mocks the search for a user in database
func (s *UserStorage) FindUser(user *user.User) error {
	if s.Error != "" {
		return errors.New(s.Error)
	}
	*user = s.Response
	return nil
}

// FindAllWithSearches mocks the eagerly fetch-all users from database
func (s *UserStorage) FindAllWithSearches() []user.User {
	return s.Responses
}
