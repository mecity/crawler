package mocks_test

import (
	"errors"

	"bitbucket.org/mecity/crawler/entities/search"
)

// SearchStorage mocks the storage where we get the stored searches
type SearchStorage struct {
	Response   *search.Search
	ReadError  string
	WriteError string
	Responses  []search.Search
}

// FindUserSearches finds all searches stored for a user
func (s *SearchStorage) FindUserSearches(userID int) []search.Search {
	return s.Responses
}

// Create mocks the creation of a search in database
func (s *SearchStorage) Create(search *search.Search) error {
	if s.WriteError != "" {
		return errors.New(s.WriteError)
	}
	return nil
}

// Delete mocks the deletion of a user's search
func (s *SearchStorage) Delete(search *search.Search, userID int) error {
	if s.WriteError != "" {
		return errors.New(s.WriteError)
	}
	return nil
}

// Update mocks the update functionality on a specific search
func (s *SearchStorage) Update(search *search.Search) (*search.Search, error) {
	if s.WriteError != "" {
		return nil, errors.New(s.WriteError)
	}
	return s.Response, nil
}

// UpdateUnread mocks the update functionality on a specific search about the unread classifieds
func (s *SearchStorage) UpdateUnread(searchID int, count int) error {
	if s.WriteError != "" {
		return errors.New(s.WriteError)
	}
	return nil
}

// FindByID mocks the query functionality for a specific search
func (s *SearchStorage) FindByID(searchID int, userID int) (*search.Search, error) {
	if s.ReadError != "" {
		return nil, errors.New(s.ReadError)
	}
	return s.Response, nil
}

// FindSearchByUser mocks the select query on searches
func (s *SearchStorage) FindSearchByUser(searchID int, userID int) (*search.Search, error) {
	if s.ReadError != "" {
		return nil, errors.New(s.ReadError)
	}
	return s.Response, nil
}
