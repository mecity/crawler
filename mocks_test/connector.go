package mocks_test

import (
	"errors"

	mocket "github.com/Selvatico/go-mocket"
	"github.com/jinzhu/gorm"
)

// Connector mocks the connector interface for unit testing
type Connector struct {
	Error string
}

// Connect creates a mock storage connection
func (s *Connector) Connect() error {
	if s.Error != "" {
		return errors.New(s.Error)
	}

	return nil
}

// Close closes a mock storage connection
func (s *Connector) Close() {}

// GetDB returns the connection to the database
func (s *Connector) GetDB() interface{} {
	mocket.Catcher.Register()
	// GORM
	db, _ := gorm.Open(mocket.DRIVER_NAME, "any_string")
	if s.Error != "" {
		db.Error = errors.New(s.Error)
	}
	return db
}
