package mocks_test

import (
	"errors"

	"bitbucket.org/mecity/crawler/entities/classified"
)

// ClassifiedStorage is the mock that implements the classified storage structure
type ClassifiedStorage struct {
	Response []classified.Classified
	Error    string
	Count    int
}

// Create mocks the creation of a classified
func (s *ClassifiedStorage) Create(classified classified.Classified) error {
	if s.Error == "" {
		return errors.New(s.Error)
	}
	return nil
}

// OpenClassified mocks the functionality of a classified when marked as read
func (s *ClassifiedStorage) OpenClassified(classifiedID int, searchID int, userID int) {
}

// CountNew mocks the functionality of counting the new classifieds of a search
func (s *ClassifiedStorage) CountNew(searchID int, userID int) int {
	return s.Count
}

// Delete mocks the deletion of a classified
func (s *ClassifiedStorage) Delete(classifiedID int, searchID int, userID int) {
}

// FindBySearch mocks the select query on classifieds by search ID
func (s *ClassifiedStorage) FindBySearch(searchID int) []classified.Classified {
	return s.Response
}

// OpenSearchClassifieds mocks the functionality to mark as read all the classifieds of a specific search
func (s *ClassifiedStorage) OpenSearchClassifieds(userID int, searchID int) {
}
