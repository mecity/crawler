FROM golang:1.13 as builder
ARG APP_NAME=api
ENV APP_NAME ${APP_NAME}
WORKDIR /go/src/bitbucket.org/mecity/crawler/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo ./cmd/$APP_NAME; done

FROM alpine:latest as certs
RUN apk --update add ca-certificates

FROM scratch
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/src/bitbucket.org/mecity/crawler/$APP_NAME .
ENTRYPOINT ["./api"]

EXPOSE 8089
