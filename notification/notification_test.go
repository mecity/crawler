package notification

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/user"
	"bitbucket.org/mecity/crawler/mocks_test"
	"bitbucket.org/mecity/crawler/notification/mail"
	"bitbucket.org/mecity/crawler/notification/push"
	"github.com/stretchr/testify/assert"
)

func TestSendNoClassifieds(t *testing.T) {
	user := &user.User{
		Email: "test@test.com",
	}
	ms := mail.NewSender()
	ps := push.NewSender()

	err := Send(user, []classified.Classified{}, ms, ps)
	assert.Nil(t, err)
}

func TestSendMailError(t *testing.T) {
	user := &user.User{
		Email: "test@test.com",
	}

	fakeServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
	}))
	ms := &mail.Sendgrid{
		Endpoint: "/v3/test_endpoint",
		Host:     fakeServer.URL,
	}
	ps := push.NewSender()

	err := Send(user, getMockClassifieds(), ms, ps)
	assert.Error(t, err)
}

func TestSendMail(t *testing.T) {
	user := &user.User{
		Email: "test@test.com",
	}

	fakeServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusAccepted)
	}))
	ms := &mail.Sendgrid{
		Endpoint: "/v3/test_endpoint",
		Host:     fakeServer.URL,
	}
	ps := push.NewSender()

	err := Send(user, getMockClassifieds(), ms, ps)
	assert.Nil(t, err)
}

func TestSendPush(t *testing.T) {
	user := &user.User{
		Email:     "test@test.com",
		Platform:  user.PlatformAndroid,
		PushToken: "test_token",
	}

	ms := mail.NewSender()
	ps := &mocks_test.PushSender{}

	err := Send(user, getMockClassifieds(), ms, ps)
	assert.Nil(t, err)
}

func TestSendPushError(t *testing.T) {
	user := &user.User{
		Email:     "test@test.com",
		Platform:  user.PlatformAndroid,
		PushToken: "test_token",
	}
	fakeServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusAccepted)
	}))
	ms := &mail.Sendgrid{
		Endpoint: "/v3/test_endpoint",
		Host:     fakeServer.URL,
	}
	ps := &mocks_test.PushSender{Error: errors.New("some error")}

	err := Send(user, getMockClassifieds(), ms, ps)
	assert.Nil(t, err)
}

func getMockClassifieds() []classified.Classified {
	return []classified.Classified{
		classified.Classified{
			ID:    1,
			Title: "Test Classified 1",
		},
		classified.Classified{
			ID:    2,
			Title: "Test Classified 2",
		},
		classified.Classified{
			ID:    3,
			Title: "Test Classified 3",
		},
	}
}
