package mail

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/mecity/crawler/entities/classified"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	s := NewSender()
	assert.NotNil(t, s)
}

func TestSendError(t *testing.T) {
	fakeServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
	}))
	s := &Sendgrid{
		Endpoint: "/v3/test_endpoint",
		Host:     fakeServer.URL,
	}
	err := s.Send("test@test.com", getMockClassifieds())
	assert.Error(t, err)
}

func TestSend(t *testing.T) {
	fakeServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusAccepted)
	}))
	s := &Sendgrid{
		Endpoint: "/v3/test_endpoint",
		Host:     fakeServer.URL,
	}
	err := s.Send("test@test.com", getMockClassifieds())
	assert.Nil(t, err)
}

func getMockClassifieds() []classified.Classified {
	return []classified.Classified{
		classified.Classified{
			ID:    1,
			Title: "Test Classified 1",
			Agent: true,
		},
		classified.Classified{
			ID:    2,
			Title: "Test Classified 2",
			Agent: false,
		},
		classified.Classified{
			ID:    3,
			Title: "Test Classified 3",
			Agent: true,
		},
	}
}
