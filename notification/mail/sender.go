package mail

import (
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/mecity/crawler/entities/classified"
	"github.com/pkg/errors"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

const (
	gmailHost  = "smtp.gmail.com"
	gmailPort  = "587"
	gmailPass  = "ZHVtbXlwb3VsaWFzMTIz"
	fromMail   = "dummypoulias@gmail.com"
	fromName   = "🏠 Mecity"
	subject    = "New Classifieds"
	templateID = "d-c3d126a0e4154ae78e8db3880005ef8e"
	chunkSize  = 30
)

// Sender is the interface that sends a mail notification via a host/provider
type Sender interface {
	Send(string, []classified.Classified) error
}

// Sendgrid is the struct that sends a mail notification via a sendgrid
type Sendgrid struct {
	Endpoint string
	Host     string
}

// NewSender creates a sender for a mail notification
func NewSender() *Sendgrid {
	return &Sendgrid{
		Endpoint: "/v3/mail/send",
		Host:     "",
	}
}

//Send is sending email via sendgrid
func (s *Sendgrid) Send(recipient string, classifieds []classified.Classified) error {

	var payload []map[string]string
	for _, v := range classifieds {
		item := map[string]string{
			"title":       v.Title,
			"description": v.Description,
			"price":       v.Price,
			"image":       v.Img,
			"url":         v.URL,
		}
		if v.Agent {
			item["agent"] = strconv.FormatBool(v.Agent)
		}
		payload = append(payload, item)
	}

	// use sendgrid api to send the email
	from := mail.NewEmail(fromName, fromMail)
	to := mail.NewEmail(recipient, recipient)
	m := mail.NewV3Mail()
	m.SetFrom(from)
	m.SetTemplateID(templateID)

	p := mail.NewPersonalization()
	p.AddTos(to)
	p.SetDynamicTemplateData("items", payload)
	m.AddPersonalizations(p)

	request := sendgrid.GetRequest(os.Getenv("SENDGRID_API_KEY"), s.Endpoint, s.Host)
	request.Method = "POST"
	client := &sendgrid.Client{Request: request}
	response, err := client.Send(m)
	if err != nil {
		return errors.Wrap(err, "sendgrid error")
	}
	if response.StatusCode >= 400 {
		res := fmt.Sprintf("sendgrid error status: %d \nsendgrid error body: %s", response.StatusCode, response.Body)
		return errors.New(res)
	}
	return nil
}
