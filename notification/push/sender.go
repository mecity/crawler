package push

import (
	"context"
	"log"

	"bitbucket.org/mecity/crawler/entities/user"
	"firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

// Sender is the interface that sends a push notification via a host/provider
type Sender interface {
	Send(*user.User, string, string) error
}

// Firebase is the struct definition of a firebase cloud messaging notification
type Firebase struct {
	credentials string
}

// NewSender creates a fcm notification structure
func NewSender() *Firebase {
	return &Firebase{
		credentials: "./firebase-private-key.json",
	}
}

// Send handles the functionality when sending a fcm notification
func (s *Firebase) Send(recipient *user.User, title, body string) error {
	app, err := firebase.NewApp(context.Background(), nil, option.WithCredentialsFile(s.credentials))
	if err != nil {
		log.Printf("Logger: Failed to initialize the notifications server: %v\n", err)
		return err
	}

	// Obtain a messaging.Client from the App.
	ctx := context.Background()
	client, err := app.Messaging(ctx)

	// This registration token comes from the client FCM SDKs.
	registrationToken := recipient.PushToken

	// See documentation on defining a message payload.
	message := &messaging.Message{
		Token: registrationToken,
		Android: &messaging.AndroidConfig{
			Priority: "normal",
			Notification: &messaging.AndroidNotification{
				Title: title,
				Body:  body,
				Color: "#0bbaaa",
			},
		},
	}

	// Send a message to the device corresponding to the provided
	// registration token.
	response, err := client.Send(ctx, message)
	if err != nil {
		log.Printf("Logger: Couldn't send notification via firebase: %v\n", err)
		return err
	}
	log.Printf("Logger: Sent push via firebase to %s and responded %s\n", recipient.Email, response)

	return nil
}
