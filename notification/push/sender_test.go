package push

import (
	"testing"

	"bitbucket.org/mecity/crawler/entities/user"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	s := NewSender()
	assert.NotNil(t, s)
}

func TestSend(t *testing.T) {
	user := &user.User{
		PushToken: "test token",
	}
	s := &Firebase{
		credentials: "../../non-existing-key.json",
	}
	err := s.Send(user, "test", "test")
	assert.Error(t, err)

	s = &Firebase{
		credentials: "../../firebase-private-key.json",
	}
	err = s.Send(user, "test", "test")
	assert.Error(t, err)
}
