package notification

import (
	"log"
	"strconv"

	"bitbucket.org/mecity/crawler/entities/classified"
	"bitbucket.org/mecity/crawler/entities/user"
	"bitbucket.org/mecity/crawler/notification/mail"
	"bitbucket.org/mecity/crawler/notification/push"
)

// Send handles the notification that must be sent.
// Currently mail or push notifications are supported. When push can't be sent, an email is sent instead.
func Send(u *user.User, classifieds []classified.Classified, ms mail.Sender, ps push.Sender) error {
	// if any classfieds found
	if len(classifieds) > 0 {

		if u.Platform == user.PlatformAndroid && len(u.PushToken) > 0 {
			title := "🏠 Mecity"
			body := "We found " + strconv.Itoa(len(classifieds)) + " new classifieds for you!"
			err := ps.Send(u, title, body)
			if err == nil {
				return nil
			}

			log.Println("Logger: Couldn't inform " + u.Email + " with firebase about new classifieds. Trying with email...")
			log.Printf("%v\n", err)
		}

		err := ms.Send(u.Email, classifieds)
		if err != nil {
			log.Println("Logger: Couldn't inform " + u.Email + " with sendgrid about new classifieds")
			log.Printf("%v\n", err)
			return err
		}

		log.Println("Logger: Informed " + u.Email + " about new classifieds")
	}
	return nil
}
