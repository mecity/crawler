# crawler
A crawler to find newly added ads for home, cars etc.
Based on [classifieds](https://bitbucket.org/sotiris_pl/classifieds) by sotiris_pl

## How to deploy the ad_crawler
Run `docker-compose up -d` to deploy the app

## On first Run
If deploying for the first time the ad_crawler container will fail because the database does not exists. Thus, you need to create the crawler database manually and restart the ad_crawler container

## Development
This up uses [fresh](https://github.com/pilu/fresh) to rebuild and rerun the app after every code change.

To develop the app using this method you need to
1. Deploy the app
2. (On first run) Create the crawler database manually
3. Just change code and save

## Run with Kubernetes
### On MacOs ([minikube](https://github.com/kubernetes/minikube))
1. Download and install Minikube
2. Run `minikube start --vm-driver=xhyve`
3. Run `eval $(minikube docker-env)`
4. Run `docker build -t ad-crawler .`
5. Run `kubectl create -f k8s/mysql-deployment.yaml`
6. Run `minikube dashboard` find the crawler-mysql pod, ssh and create the crawler DB  
7. Run `kubectl create -f k8s/crawler-deployment.yaml`
8. Run `minikube service --url crawler` to get the Crawler API URL

## Lint code before push
Run for the first time `go get -u golang.org/x/lint/golint`
Then to lint code: 
```
golint -set_exit_status=1 `go list ./...`
```

## Test suite
```
go test ./... -race
```
 ### Coverage
```
go test -coverprofile cover.out ./...; go tool cover -html=cover.out
```
